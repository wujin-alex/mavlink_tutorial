import socket
import time

BUFSIZE = 1024
def main():
    ip_port = ('', 14540)
    server2 = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server2.bind(ip_port)

    des_addr = ('127.0.0.1', 14550)
    while True:
        data_send = b"Hello, this server2: "
        server2.sendto(data_send, des_addr)

        data, addr = server2.recvfrom(BUFSIZE)
        print('Recived from {}:'.format(addr))
        print(data)
        time.sleep(1)

    server2.close()
if __name__ == '__main__':
    main()