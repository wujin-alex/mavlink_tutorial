import socket
import time

BUFSIZE = 1024
def main():
    client = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)

    ip_port = ('127.0.0.1', 14550)
    client.sendto(b"hello this is udp client",ip_port)
    data, addr = client.recvfrom(BUFSIZE)
    print("Received from: {}".format(addr))
    print(data)
    
    client.close()

if __name__ == '__main__':
    main()