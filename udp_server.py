import socket
import time

BUFSIZE = 1024
def main():
    ip_port = ('', 14550)
    server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server.bind(ip_port)

    while True:
        data, addr = server.recvfrom(BUFSIZE)
        print('Received frome {}:'.format(addr))
        print(data)
        response = b"Hello, this server1, I received: "
        server.sendto(response + data, addr)

    server.close()
if __name__ == '__main__':
    main()
