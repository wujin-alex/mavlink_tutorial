#!/usr/bin/env python3
import time
import socket
from mavlink import *

class DeviceUdp(object):
    """
    """
    def __init__(self, local_addr, target_addr=None):
        """
        local_port: local port to receive data
        For example, target_addr = ('192.168.4.1', 18750)
        """
        self.dev         = None
        self.local_addr  = local_addr
        self.target_addr = target_addr

        self.dev = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.dev.bind(self.local_addr)

    def write(self, buf):
        send_callback = self.dev.sendto(buf, self.target_addr)
    def read(self, size):
        buf, source_addr = self.dev.recvfrom(size)
        return buf

local_addr  = ('', 14550)
target_addr = ('127.0.0.1', 14540)
dev = DeviceUdp(local_addr, target_addr)

sys_id = 1
cmp_id = 1
mav = MAVLink(dev, sys_id, cmp_id)

while True:
    data = dev.read(256)
    msg = None
    if len(data)>0:
        try:
            msg = mav.parse_char(data)
        except Exception as e:
            continue
        if isinstance(msg, MAVLink_message):
            print("INFO: Got a mavlink message")

        if isinstance(msg, MAVLink_heartbeat_message):
            print("INFO: sys={}, cmp={}".format(msg.get_srcSystem(), msg.get_srcComponent()))
            print("INFO: type={}, autopilot={}".format(msg.type, msg.autopilot))
                    


