#!/usr/bin/env python3
import time
import socket
from mavlink import *

class DeviceUdp(object):
    """
    """
    def __init__(self, local_addr, target_addr=None):
        """
        local_port: local port to receive data
        For example, target_addr = ('192.168.4.1', 18750)
        """
        self.dev         = None
        self.local_addr  = local_addr
        self.target_addr = target_addr

        self.dev = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.dev.bind(self.local_addr)

    def write(self, buf):
        send_callback = self.dev.sendto(buf, self.target_addr)

local_addr  = ('', 14540)
target_addr = ('127.0.0.1', 14550)
dev = DeviceUdp(local_addr, target_addr)

sys_id = 1
cmp_id = 1
mav = MAVLink(dev, sys_id, cmp_id)

for i in range(10):
    msg = MAVLink_heartbeat_message(MAV_TYPE_QUADROTOR, 
            MAV_AUTOPILOT_PX4, MAV_MODE_PREFLIGHT, 0x60000, MAV_STATE_STANDBY, 2)
    mav.send(msg)
    time.sleep(1)
    print("Send heartbeat {}-th".format(i))

